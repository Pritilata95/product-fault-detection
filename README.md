# Detect Product Faults with a Smart Quality System

### Made With : ![Python](https://img.shields.io/badge/python-3670A0?style=for-the-badge&logo=python&logoColor=ffdd54) ![Jupyter Notebook](https://img.shields.io/badge/jupyter-%23FA0F00.svg?style=for-the-badge&logo=jupyter&logoColor=white) ![Pandas](https://img.shields.io/badge/pandas-%23150458.svg?style=for-the-badge&logo=pandas&logoColor=white) ![NumPy](https://img.shields.io/badge/numpy-%23013243.svg?style=for-the-badge&logo=numpy&logoColor=white) ![SciPy](https://img.shields.io/badge/SciPy-%230C55A5.svg?style=for-the-badge&logo=scipy&logoColor=%white) ![scikit-learn](https://img.shields.io/badge/scikit--learn-%23F7931E.svg?style=for-the-badge&logo=scikit-learn&logoColor=white) 

### Code Metrics : [![Codacy Badge](https://app.codacy.com/project/badge/Grade/f4a9f4a2577e4e9f93cf539985ea7454)](https://www.codacy.com/gl/abhirup_sinha/product-fault-detection/dashboard?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=abhirup_sinha/product-fault-detection&amp;utm_campaign=Badge_Grade)

### Task
Developing a monitoring system, that identifies the quality of the printing on the product using the historical vibration data and the quality and production log data. 

### Description of the data set

There is data for one bearing with 2 acceleration sensors. The data set consists of individual files that are 1 second vibration signal snapshots recorded at specific intervals. Each file consists of a time series with 20480 points at a sampling rate of ~20 kHz. The file name indicates when the data was collected. Each record (file) represents a production process and a corresponding product (row in production or quality log respectively). 

### Authors

Abhirup Sinha 
Pritilata Saha  
Johannes Kaiser  
Abhilash Kandarpa  
Sachin Kumar  

### Contact

Repo Owner (mailme@abhirupsinha.com) or Pritilata (pritilata@latasaha.com)
